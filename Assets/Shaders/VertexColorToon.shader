Shader "VertexColorToon"
{
	Properties
	{
		_Color("MixColor", Color) = (1,1,1,1)
		_AColor("AdditiveColor", Color) = (0,0,0,0)
		[NoScaleOffset] _Gradient("Gradient", 2D) = "white" {}
	}
	SubShader
	{
		Cull off
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			Tags{ "LightMode" = "ForwardBase" }
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"
			#include "AutoLight.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float4 normal : NORMAL;
				fixed4 color : COLOR;
			};

			struct v2f
			{
				SHADOW_COORDS(1)
				UNITY_FOG_COORDS(2)
				float4 vertex : SV_POSITION;
				fixed4 vcolor : COLOR0;
				fixed3 color : COLOR1;

			};
			fixed4 _Color;
			fixed4 _AColor;
			sampler2D _Gradient;
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.vcolor = v.color*_Color+_AColor;
				half3 worldNormal = UnityObjectToWorldNormal(v.normal);
				o.color = max(0.005, dot(worldNormal, _WorldSpaceLightPos0.xyz));
				TRANSFER_SHADOW(o);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed atten = SHADOW_ATTENUATION(i);
				fixed4 col = tex2D(_Gradient,i.color)* i.vcolor*atten;
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
		UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
	}
}

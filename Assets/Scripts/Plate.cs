﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plate : MonoBehaviour {
    public Color triggeredcolor;
    public float speed;
    public float timeout;
    public Interactive interactive;
    Material mat;
    bool trigger;
    Color defaultcolor;
    float timecount;
    // Use this for initialization
    void Start () {
        mat = GetComponent<Renderer>().material;
        defaultcolor = mat.color;
	}
	
	// Update is called once per frame
	void Update () {
		if(!trigger) mat.color = Color.Lerp(mat.color, defaultcolor, speed * Time.deltaTime);
    }
    private void OnTriggerEnter(Collider other)
    {
        trigger = true;
        timecount = timeout;
    }
    private void OnTriggerStay(Collider other)
    {
        timecount -= Time.deltaTime;
        mat.color = Color.Lerp(mat.color, triggeredcolor, speed * Time.deltaTime);
        if (timecount < 0 &&enabled)
        {
            interactive.Trigger();
            enabled = false;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        trigger = false;
    }

}

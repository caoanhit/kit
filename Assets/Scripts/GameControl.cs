﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControl : MonoBehaviour {
    public CameraFollow cam;
    public SwipeControl control;
    public CharacterMovement fox;
    public float delay;
    public float camdelay;
    public GameObject startingtext;

    bool started = false;
	// Use this for initialization
	void Awake () {
        control.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0)&&!started)
        {
            StartGame();
            started = true;
        }
	}
    public void StartGame()
    {
        StartCoroutine(Ready());
    }
    IEnumerator Ready()
    {
        startingtext.SetActive(false);
        fox.Ready();
        yield return new WaitForSeconds(delay);
        cam.StartGame();
        yield return new WaitForSeconds(camdelay);
        control.enabled = true;
    }
}

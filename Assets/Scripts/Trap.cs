﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour {

    Collider col;
	// Use this for initialization
	void Start () {
        col = GetComponent<Collider>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void Enable()
    {
        col.enabled = true;
    }
    public void Disable()
    {
        col.enabled = false;
    }
}

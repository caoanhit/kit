﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eye : MonoBehaviour {
    public GameObject eye;

    Animator anim;
    private void Start()
    {
        anim = eye.GetComponent<Animator>();
    }
    public void Open()
    {
        anim.SetTrigger("Open");
    }
}

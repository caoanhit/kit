﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    public float smoothtime=0.3f;
    public Transform target;
    public Vector3 targetOffset;
    public Vector3 startRotationOffset;
    public Vector3 rotationOffset;
    public float moveSpeed;
    public float rotationSpeed;
    public float focusSpeed;
    //public Vector2 xlimit;
    //public Vector2 zlimit;

    Vector3 offset;
    Vector3 rotOffset;
    Vector3 velocity = Vector3.zero;
    Vector3 shakeoffset = Vector3.zero;
    bool following=false;
    IEnumerator CamShake;

    // Use this for initialization
    void Start () {
        offset = targetOffset;
        rotOffset = startRotationOffset;
        transform.forward = target.position + startRotationOffset - transform.position;
    }
	
	// Update is called once per frame
	void LateUpdate () { 
        Vector3 tar = target.position + offset+shakeoffset;
        //if (tar.x < xlimit.x) tar.x = xlimit.x;
        //else if (tar.x > xlimit.y) tar.x = xlimit.y;
        //if (tar.z < zlimit.x) tar.z = zlimit.x;
        //else if (tar.z > zlimit.y) tar.z = zlimit.y;
        if (following)
            transform.position = Vector3.SmoothDamp(transform.position, tar, ref velocity, smoothtime);
	}
    public void Follow()
    {
        following = true;
    }
    public void UnFollow()
    {
        following = false;
    }
    IEnumerator IShake(Vector3 shakeamount, float delay)
    {
        while (true)
        {
            shakeoffset = new Vector3(Random.Range(shakeamount.x, -shakeamount.x), Random.Range(shakeamount.y, -shakeamount.y), Random.Range(shakeamount.z, -shakeamount.z));
            yield return new WaitForSeconds(delay);
        }
    }
    public void StartShake(Vector3 shakeamount, float delay)
    {
        CamShake = IShake(shakeamount, delay);
        StartCoroutine(CamShake);
    }
    public void StopShake()
    {
        if (CamShake != null) StopCoroutine(CamShake);
        shakeoffset = Vector3.zero;
    }
    IEnumerator Ready()
    {
        Vector3 destination= target.position + offset;
        while (Vector3.Distance(transform.position, destination) > 0.1f)
        {
            transform.position = Vector3.MoveTowards(transform.position, destination, moveSpeed * Time.deltaTime);
            rotOffset = Vector3.MoveTowards(rotOffset, rotationOffset, focusSpeed * Time.deltaTime);
            transform.forward = Vector3.MoveTowards(transform.forward, target.position+rotOffset- transform.position, rotationSpeed * Time.deltaTime);
            yield return null;
        }
        following = true;
    }
    public void StartGame()
    {
        StartCoroutine(Ready());
    }
}

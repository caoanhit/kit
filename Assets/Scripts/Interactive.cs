﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactive : MonoBehaviour {
    public ParticleSystem[] particles;
    
    Animator anim;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void Trigger()
    {
        anim.SetTrigger("Trigger");
        foreach (ParticleSystem par in particles)
        {
            par.Play();
        }
    }
}
